# XLectric XNAT Assistant #

This is a proof-of-concept application for the following technologies:

* Node.js
* Electron
* node-java
* XNAT Mizer Integration

This application has very limited functionality at the moment. It can:

* Find DICOM session(s) in a specified source folder and display the results
* Copy the files from a session to a target folder
* "Anonymize" the copied DICOM data by modifying one of the header values

## Building and Running ##

1. If you haven't already, clone this repo then cd into the cloned folder:

    ```bash
    $ git clone git@bitbucket.org:rherrick/xlectric.git
    $ cd xlectric
    ```

1. Run the script **scripts/mizer-paths.sh**. This downloads the Java jars required for the anonymization operation into the **libs** folder. Also, the script uses [httpie](https://httpie.org), so you'd either need to install that or modify the script to use **wget** or **curl** (you should install **httpie**, though, it's a great application).

1. Download and configure the **node** module dependencies:

    ```bash
    $ yarn install
    ```

    I had a fair amount of trouble trying to use **npm** to manage the app, so I used yarn instead. You're welcome to try npm if you're more comfortable with that, though.

1. The app should now be ready to run. You can run it on OS X like so:

    ```bash
    $ ./node_modules/electron/dist/Electron.app/Contents/MacOS/Electron --remote-debugging-port=9222 .
    ```
    On Windows or Linux, it would be similar but would use a different path for the **dist** in place of **Electron.app**.

## Validating ##

You can validate the function of the application by downloading a [sample DICOM session](https://download.xnat.org/images/sample2-slim.zip) and copying it with the application.

1. Download the zip file containing the sample DICOM data.
1. Extract the contents of the zip.
1. Create a new folder to be the target for the copy operation.
1. In the application, specify the extracted **sample2-slim** folder as the **Source** folder.
1. Specify your new target directory as the **Target** folder.
1. Click the **Find Sessions** button.
1. You should see a single item entitled **XNAT_01** in the list below. Click on that.
1. Click **Copy to target**.

There are two ways to verify that this worked properly and that the data was properly edited.

1. There are 84 files in the **sample2-slim** session. There should be 84 files in your target folder as well:

    ```bash
    $ find sample2-slim -type f | wc -l
      84
    $ find anonymized -type f | wc -l
      84
    ```

1. Use **dcmdump** from the [dcmtk application suite](http://dicom.offis.de/dcmtk.php.en) (see note below) to view the actual DICOM header values. The only change that the application makes to the DICOM is to the **Manufacturer** header, which is tag 0008,0070. You can view the differences between the two like this:

    ```bash
    $ find sample2-slim -type f | xargs dcmdump +P 0008,0070 | sort -u

    (0008,0070) LO [No site anonymization performed]        #  32, 1 Manufacturer
    $ find anonymized -type f | xargs dcmdump +P 0008,0070 | sort -u

    (0008,0070) LO [Just changed it]                        #  16, 1 Manufacturer
    ```

#### Note
> The DCMTK page has a number of downloadable packages, but you probably really want to use an installer for your
> particular platform. If you're on OS X, the easiest way to do this is through [Homebrew](https://brew.sh/):
>
> ```bash
> $ brew install dcmtk
> ```
>
> On Debian platforms you can install **dcmtk** using **apt-get** or **apt** if you configure [NeuroDebian](http://neuro.debian.net).
> I'm not sure what options there are for Fedora-based platforms or Windows.
