const {shell, remote, ipcRenderer} = require('electron');
const dicomParser = require('dicom-parser');
const dir = require('node-dir');
const fs = require('fs');
const mkdirp = require('mkdirp');
const loki = require('lokijs');
const path = require('path');
const logger = require('./logger');

logger.info('Starting XLectric Renderer');

const mainProcess = remote.require('./main.js');

const db = new loki('xlectric.json');
const studies = db.addCollection('studies', {'unique': ['studyInstanceUid'], 'autoupdate': true});

const sessions = document.querySelector('.sessions');
const messageDisplay = document.querySelector('.message-display');
const sessionSourceText = document.querySelector('#session-source-text');
const sessionSourceSelect = document.querySelector('.select-source-button');
const sessionTargetText = document.querySelector('#session-target-text');
const sessionTargetSelect = document.querySelector('.select-target-button');
const sessionFolderSearch = document.querySelector('.find-sessions-button');
const sessionFolderCopy = document.querySelector('.copy-to-target-button');
const clearSessionsButton = document.querySelector('.clear-sessions-button');

sessions.addEventListener('click', (event) => {
    if (event.target.href) {
        event.preventDefault();
        shell.openExternal(event.target.href);
    }
});

ipcRenderer.on('folder-selected', (event, name, folder) => {
    const targetText = name === 'source' ? sessionSourceText : sessionTargetText;
    targetText.value = folder;
    sessionFolderSearch.disabled = getSourceFolder() === '';
    sessionFolderCopy.disabled = getTargetFolder() === '';
});

sessionSourceSelect.addEventListener('click', (event) => {
    event.preventDefault();
    mainProcess.selectFolder('source');
});

sessionTargetSelect.addEventListener('click', (event) => {
    event.preventDefault();
    mainProcess.selectFolder('target', true);
});

sessionFolderSearch.addEventListener('click', (event) => {
    event.preventDefault();

    const folder = getSourceFolder();
    if (!folder) {
        handleError("No source folder specified, can't search for sessions");
        return;
    }

    displayMessage(`Now processing the folder ${folder}`);

    try {
        dir.files(folder, findSessions);
    } catch (error) {
        handleError(`There was an error processing the path ${folder}`, error);
    }
});

sessionFolderCopy.addEventListener('click', (event) => {
    event.preventDefault();

    const sourceFolder = getSourceFolder();
    if (!sourceFolder) {
        handleError("No source folder specified, can't copy sessions");
        return;
    }
    if (getStudyCount() === 0) {
        handleError("You must search for sessions in a folder containing valid DICOM before attempting to copy.");
        return;
    }
    const targetFolder = getTargetFolder();
    if (!targetFolder) {
        handleError("No target folder specified, can't copy sessions");
        return;
    }

    displayMessage(`Now copying DICOM files from ${sourceFolder} to ${targetFolder}`);

    copySessions();
});

clearSessionsButton.addEventListener('click', () => {
    studies.clear();
    renderStudies();
});

const getSourceFolder = () => {
    return sessionSourceText.value;
};

const getTargetFolder = () => {
    return sessionTargetText.value;
};

const findSessions = (error, files) => {
    if (error) {
        throw error;
    }

    const sourceFolder = getSourceFolder();

    let count = 0;
    let errors = 0;
    let warnings = 0;
    files.forEach(file => {
        displayMessage(`Reading file ${file} (#${++count}`);

        try {
            const dicomFile = fs.readFileSync(file);
            const dicom = dicomParser.parseDicom(dicomFile, {untilTag: '0x00324000'});
            const studyDescription = dicom.string('x00081030');
            const studyInstanceUid = dicom.string('x0020000d');
            const seriesDescription = dicom.string('x0008103e');
            const seriesInstanceUid = dicom.string('x0020000e');
            const seriesNumber = dicom.string('x00200011');

            addMessage(`: got description "${studyDescription}", study instance UID ${studyInstanceUid}`);

            saveStudy(studyInstanceUid, studyDescription, sourceFolder, seriesNumber, seriesInstanceUid, seriesDescription, file);
        } catch (error) {
            handleError(`There was an error processing the file ${file}`, error);
            errors++;
        }
    });
    displayMessage(`Processed ${count} files total with ${errors} errors and ${warnings} warnings`);
    renderStudies();
};

const copySessions = (studyInstanceUids = []) => {
    const sourceFolder = getSourceFolder();
    const targetFolder = getTargetFolder();

    const filePaths = getFilePaths(studyInstanceUids);
    filePaths.forEach(filePath => {
        const source = path.join(sourceFolder, filePath);
        const target = path.join(targetFolder, filePath);
        const targetDir = path.parse(target)['dir'];

        // Make sure the target directory exists.
        if (!fs.existsSync(targetDir)) {
            mkdirp(targetDir, function (err) {
                if (err) {
                    logger.error("An error occurred trying to create the directory " + targetDir + ": " + err);
                }
            });
        }

        let readStream = fs.createReadStream(source);

        readStream.once('error', (error) => {
            handleError(`An error occurred trying to copy the file ${source} to ${targetDir}`, error);
        });

        readStream.once('end', () => {
            displayMessage(`Copied ${source} to ${targetDir}`);
        });

        readStream.pipe(fs.createWriteStream(target));

        try {
            mainProcess.anonymize(target, '(0008,0070) := "Just changed it"');
        } catch (error) {
            logger.error("An error occurred during anonymization: " + error);
        }
    });
};

/**
 * Indicates whether any studies are stored in the database.
 *
 * @returns {boolean}
 */
const hasStudies = () => {
    return getStudyCount() > 0;
};

/**
 * Indicates whether the particular study instance UID exists in the database.
 *
 * @param studyInstanceUid The study instance UID to search for.
 * @returns {boolean}
 */
const hasStudy = (studyInstanceUid) => {
    return getStudyCount(studyInstanceUid) > 0;
};

/**
 * Returns the number of studies in the database. If a study instance UID is specified, the result will be either 0 or
 * 1. This is used by the {@link #hasStudy()} method.
 *
 * @param studyInstanceUid An optional study instance UID.
 *
 * @returns {int}
 */
const getStudyCount = (studyInstanceUid = '') => {
    return studyInstanceUid ? studies.count({'studyInstanceUid': studyInstanceUid}) : studies.count();
};

/**
 * Returns all file paths stored in the database. If one or more study instance UIDs is specified, only file paths
 * associated with those sessions are returned.
 *
 * @param studyInstanceUids One or more study instance UIDs (optional).
 */
const getFilePaths = (studyInstanceUids = []) => {
    // Base query on whether or not we got any studies.
    let results = studyInstanceUids.length > 0 ? studies.find({'studyInstanceUid': {'$in': studyInstanceUids}}) : studies.find();
    return results.map(study => study.series)
        .map(series => Object.values(series))
        .reduce((acc, cur) => acc.concat(cur), [])
        .reduce((acc, cur) => acc.concat(cur.files), []);
};

const saveStudy = (studyInstanceUid, studyDescription, sourceFolder, seriesNumber, seriesInstanceUid, seriesDescription, file) => {
    displayMessage(`Saving study ${studyDescription} series ${seriesNumber}: ${seriesDescription}`);
    const study = getOrCreateStudy(studyInstanceUid, studyDescription, sourceFolder);
    const series = study['series'];
    const relative = path.relative(sourceFolder, file);
    if (!(seriesNumber in series)) {
        series[seriesNumber] = {
            'seriesNumber': seriesNumber,
            'seriesInstanceUid': seriesInstanceUid,
            'seriesDescription': seriesDescription,
            'files': [relative]
        };
    } else {
        series[seriesNumber].files.push(relative);
    }

    // Don't need to do explicit update if we have the study already because autoupdate is on.
    if (!hasStudy(studyInstanceUid)) {
        studies.insert(study);
    }
};

const getOrCreateStudy = (studyInstanceUid, studyDescription, sourceFolder) => {
    const existing = studies.findOne({'studyInstanceUid': studyInstanceUid});
    return existing || {
        'studyInstanceUid': studyInstanceUid,
        'studyDescription': studyDescription,
        'sourceFolder': sourceFolder,
        'series': new Map()
    };
};

const convertToElement = (study, index) => {
    let className = `accordion${index}`;
    let element = `<tr data-toggle="collapse" data-target=".${className}" class="clickable"><td colspan='3'>${study.studyDescription}</td><td>${study.studyInstanceUid}</td></tr>`;
    const series = study['series'];
    Object.keys(series).forEach(key => {
        const seriesInfo = series[key];
        element += `<tr class="${className} collapse"><td>${seriesInfo.seriesNumber}</td><td>${seriesInfo.seriesDescription}</td><td>${seriesInfo.seriesInstanceUid}</td><td>${seriesInfo.files.length} files</td></tr>`;
    });
    return element;
};

const handleError = (message, error = '') => {
    if (!error) {
        displayMessage(message, true);
    } else if (!error.message) {
        displayMessage(`${message}: ${error}`.trim(), true);
    } else {
        displayMessage(`${message}: ${error.message}`.trim(), true);
    }
};

const displayMessage = (text = '', isError = false) => {
    messageDisplay.innerHTML = text;
    if (isError) {
        sessionSourceText.classList.add('error');
        logger.error('An error occurred: ' + text);
    } else {
        sessionSourceText.classList.remove('error');
    }
};

const addMessage = (text) => {
    messageDisplay.innerHTML += text;
};

const renderStudies = () => {
    try {
        sessions.innerHTML = hasStudies() ? studies.find().map(convertToElement).join('') : '(no sessions found)';
        if (hasStudies) {
            sessions.classList.remove('none');
        } else {
            sessions.classList.add('none');
        }
        displayMessage('');
        clearSessionsButton.disabled = !hasStudies;
    } catch (error) {
        handleError("There was an error trying to render the studies list", error);
    }
};

renderStudies();
