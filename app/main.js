const {app, BrowserWindow, dialog} = require('electron', 'dicomParser');
const mizer = require('./mizer');
const java = require('java');
const logger = require('./logger');

let mainWindow = null;

logger.info('Starting the XLectric application');

app.on('ready', () => {
    logger.info('XLectric starting up');

    mainWindow = new BrowserWindow({show: false});

    mainWindow.loadURL(`file://${__dirname}/index.html`);

    // mainWindow.webContents.openDevTools();

    const path = java.callStaticMethodSync("java.nio.file.Paths", "get", java.newInstanceSync("java.lang.String"));
    logger.debug(`Java says the current working folder is ${path.toAbsolutePathSync().toStringSync()}`);

    mainWindow.once('ready-to-show', () => {
        mainWindow.show();
    });

    mainWindow.on('closed', () => {
        mainWindow = null;
    });
});

exports.selectFolder = (name, allowCreate = false) => {
    const properties = ['openDirectory'];
    if (allowCreate) {
        properties.push('createDirectory');
    }
    const folder = dialog.showOpenDialog(mainWindow, {
        properties: properties
    });
    if (!folder || folder.length === 0) {
        return;
    }
    logger.debug(`User selected folder ${folder} for ${name}`);
    mainWindow.webContents.send('folder-selected', name, folder[0]);
};

exports.anonymize = (source, script, variables) => {
    mizer.anonymize(source, script, variables);
};