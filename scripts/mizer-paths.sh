#!/usr/bin/env bash
http -b --download --output libs/guava-20.0.jar https://repo1.maven.org/maven2/com/google/guava/guava/20.0/guava-20.0.jar
http -b --download --output libs/commons-io-2.5.jar https://repo1.maven.org/maven2/commons-io/commons-io/2.5/commons-io-2.5.jar
http -b --download --output libs/commons-logging-1.2.jar https://repo1.maven.org/maven2/commons-logging/commons-logging/1.2/commons-logging-1.2.jar
http -b --download --output libs/slf4j-log4j12-1.7.25.jar https://repo1.maven.org/maven2/org/slf4j/slf4j-log4j12/1.7.25/slf4j-log4j12-1.7.25.jar
http -b --download --output libs/log4j-1.2.17.jar https://repo1.maven.org/maven2/log4j/log4j/1.2.17/log4j-1.2.17.jar
http -b --download --output libs/antlr-runtime-3.5.2.jar https://repo1.maven.org/maven2/org/antlr/antlr-runtime/3.5.2/antlr-runtime-3.5.2.jar
http -b --download --output libs/antlr4-4.5.3.jar https://repo1.maven.org/maven2/org/antlr/antlr4/4.5.3/antlr4-4.5.3.jar
http -b --download --output libs/commons-lang3-3.5.jar https://repo1.maven.org/maven2/org/apache/commons/commons-lang3/3.5/commons-lang3-3.5.jar
http -b --download --output libs/lombok-1.16.18.jar https://repo1.maven.org/maven2/org/projectlombok/lombok/1.16.18/lombok-1.16.18.jar
http -b --download --output libs/reflections-0.9.10.jar https://repo1.maven.org/maven2/org/reflections/reflections/0.9.10/reflections-0.9.10.jar
http -b --download --output libs/slf4j-api-1.7.25.jar https://repo1.maven.org/maven2/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar
http -b --download --output libs/spring-core-4.3.9.RELEASE.jar https://repo1.maven.org/maven2/org/springframework/spring-core/4.3.9.RELEASE/spring-core-4.3.9.RELEASE.jar

http -b --download --output libs/dcm4che-core-2.0.29.jar http://www.dcm4che.org/maven2/dcm4che/dcm4che-core/2.0.29/dcm4che-core-2.0.29.jar
http -b --download --output libs/dcm4che-iod-2.0.29.jar http://www.dcm4che.org/maven2/dcm4che/dcm4che-iod/2.0.29/dcm4che-iod-2.0.29.jar
http -b --download --output libs/dcm4che-net-2.0.29.jar http://www.dcm4che.org/maven2/dcm4che/dcm4che-net/2.0.29/dcm4che-net-2.0.29.jar

http -b --download --output libs/dicom-edit4-1.0.2-SNAPSHOT.jar https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot/org/nrg/dicom/dicom-edit4/1.0.2-SNAPSHOT/dicom-edit4-1.0.2-SNAPSHOT.jar
http -b --download --output libs/dicom-edit6-1.0.2-SNAPSHOT.jar https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot/org/nrg/dicom/dicom-edit6/1.0.2-SNAPSHOT/dicom-edit6-1.0.2-SNAPSHOT.jar
http -b --download --output libs/mizer-1.0.2-SNAPSHOT.jar https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot/org/nrg/dicom/mizer/1.0.2-SNAPSHOT/mizer-1.0.2-SNAPSHOT.jar
http -b --download --output libs/dicomtools-1.7.4.jar https://nrgxnat.jfrog.io/nrgxnat/libs-release/org/nrg/dicomtools/1.7.4/dicomtools-1.7.4.jar
http -b --download --output libs/framework-1.7.4.jar https://nrgxnat.jfrog.io/nrgxnat/libs-release/org/nrg/framework/1.7.4/framework-1.7.4.jar
http -b --download --output libs/transaction-1.7.4.jar https://nrgxnat.jfrog.io/nrgxnat/libs-release/org/nrg/transaction/1.7.4/transaction-1.7.4.jar
